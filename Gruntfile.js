module.exports = function(grunt) {

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
      imagemin: {                          
        dynamic: {                         
          files: [{
            expand: true,
            cwd: 'images/orig/',
            src: ['**/*.{png,jpg,gif}'],
            dest: 'images/comp/'
          }]
        }
      },
      sass: {                              // Task
        dist: {                            // Target
          options: {                       // Target options
            style: 'expanded'
          },
          files: {                         // Dictionary of files
            'styles/css/styles.css': 'styles/scss/styles.scss',       // 'destination': 'source'
          }
        }
      },
      watch: {
        files: ['styles/scss/*'],
        tasks: ['sass']
      }
      
      
  });
  grunt.loadNpmTasks('grunt-contrib-imagemin');
  grunt.loadNpmTasks('grunt-contrib-sass');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.registerTask('default', ['imagemin', 'sass'] );
};